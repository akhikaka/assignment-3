#!/bin/bash

#clear
#echo "Enter a string to be entered:"
#read str
#echo

str=$1

len=`echo $str | wc -c`
len=`expr $len - 1`
i=1
j=`expr $len / 2`
while test $i -le $j
do
k=`echo $str | cut -c $i`
l=`echo $str | cut -c $len`
if test $k != $l
then
echo "Provided string \"$str\" is not a palindrome"
exit
fi
i=`expr $i + 1`
len=`expr $len - 1`
done
echo "Provided string \"$str\" is a palindrome"
