# STEPS
1. `chmod a+x palindrome.sh`
2. Add desired words to check if it is a palindrome
```
sh palindrome.sh level
sh palindrome.sh supercalifragilisticexpialidocious
sh palindrome.sh tattarrattat
```